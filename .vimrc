set nocompatible
set modelines=0
set hidden
set undofile
set undodir=~/.vim/tmp"
set directory^=~/.vim/tmp/
set wildmode=longest,list,full
set wildmenu
set wildignorecase
set ignorecase
set smartcase
set infercase
set gdefault
set backspace=indent,eol,start
set hlsearch
set incsearch
set shiftwidth=2
set expandtab
set tabstop=2
set softtabstop=2
set autoindent
set showmatch
set shortmess+=I
filetype off
set lazyredraw
set number
set relativenumber
set nowrap
set nofoldenable
set foldlevel=99
set foldminlines=99
set foldlevelstart=99
set list
set listchars=
set listchars+=tab:𐄙\ 
set listchars+=trail:·
set listchars+=extends:»
set listchars+=precedes:«
set listchars+=nbsp:⣿
set clipboard=unnamedplus
set laststatus=2
set noshowmode
set exrc
set secure

autocmd FileType c,cpp,java,php,js,python,twig,xml,yml autocmd BufWritePre <buffer> :call setline(1,map(getline(1,"$"),'substitute(v:val,"\\s\\+$","","")'))

call plug#begin('~/.vim/plugged')

Plug 'rakr/vim-one'
Plug 'itchyny/lightline.vim'
Plug 'sheerun/vim-polyglot'
Plug 'scrooloose/nerdcommenter'
Plug 'dense-analysis/ale'
Plug 'Lokaltog/vim-easymotion'
Plug 'tpope/vim-surround'
Plug 'jiangmiao/auto-pairs'
Plug 'ryanoasis/vim-devicons'
Plug 'ajh17/VimCompletesMe'
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'junegunn/vim-easy-align'

call plug#end()

colorscheme one
set background=dark

call one#highlight('NonText', 'ffffff', '', 'none')
call one#highlight('SpecialKey', 'ffffff', '', 'none')

let g:lightline = {
                  \ 'colorscheme': 'one',
                  \ 'active': {
                  \   'left': [ [ 'mode', 'paste' ],
                  \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
                  \ },
                  \ 'separator': { 'left': '', 'right': '' }, 
                  \ 'subseparator': { 'left': '\\', 'right': '/' } 
                  \ }

xmap ga <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)

map gb :bnext<cr>
map gB :bprevious<cr>

nnoremap <leader>w :w!<cr>
nnoremap <leader>r :redo<cr>
nnoremap <leader>qa :qa!<cr>
nnoremap <leader>q :q<cr>
nnoremap <leader>b :bd<CR>
nnoremap <leader>t :tabclose<cr>
nnoremap <leader>i gg=G ''
nnoremap <tab> <C-w><C-w>
nnoremap <leader>fe :NERDTreeToggle<cr>
nnoremap <leader>fx :ALEFix prettier<cr>

noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>
